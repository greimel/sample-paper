# Sample paper

Pipeline Status: [![pipeline status](https://gitlab.com/greimel/sample-paper/badges/customizing/pipeline.svg)](https://gitlab.com/greimel/sample-paper/commits/main)

Download the 
[paper](https://gitlab.com/greimel/sample-paper/-/jobs/artifacts/main/raw/paper/paper.pdf?job=pdfs), the 
[slides](https://gitlab.com/greimel/sample-paper/-/jobs/artifacts/main/raw/slides/slides.pdf?job=pdfs) and the
[sample Pluto notebook](https://gitlab.com/greimel/sample-paper/-/jobs/artifacts/main/file/notebooks/notebook.html?job=julia).

