using Pkg
Pkg.activate(temp = true)
Pkg.add(["Pluto", "LoggingExtras", "Dates"])

# --------------------------------------------------------------------
# ----------------  Export html from Pluto notebooks  ----------------
# --------------------------------------------------------------------

import Pluto

function export_html(nbfile, htmlfile)
    s = Pluto.ServerSession()
    
    nbfile = 
    
    nb = Pluto.SessionActions.open(s, nbfile; run_async=false)
    
    html_contents = Pluto.generate_html(nb)
    
    write(htmlfile, html_contents)
    
    Pluto.SessionActions.shutdown(s, nb)
end

# --------------------------------------------------------------------
# -------------------  Adding time stamps to logs  -------------------
# --------------------------------------------------------------------

using LoggingExtras, Dates

const date_format = "HH:MM:SS"

timestamp_logger(logger) = TransformerLogger(logger) do log
  merge(log, (; message = "$(Dates.format(now(), date_format)) $(log.message)"))
end

ConsoleLogger(stdout, Logging.Debug) |> timestamp_logger |> global_logger

# --------------------------------------------------------------------
# -------------------  Environment and file paths  -------------------
# --------------------------------------------------------------------

projectdir(args...)   = joinpath(@__DIR__(), "..", args...) |> normpath
scriptsdir(args...)   = projectdir("scripts", args...)
notebooksdir(args...) = projectdir("notebooks", args...)

isdir(notebooksdir()) || mkdir(notebooksdir())

# --------------------------------------------------------------------
# --------------------  Run notebooks and scripts  -------------------
# --------------------------------------------------------------------

for nb in ["notebook"]
    @info "Exporting notebook $(nb).jl"
    
    export_html(
        scriptsdir(nb * ".jl"),
        notebooksdir(nb * ".html")
    )
end